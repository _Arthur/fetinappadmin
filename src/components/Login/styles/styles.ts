import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  height: 100vh;

  display: flex;
  justify-content: center;
  /* align-items: center; */

  form {
    width: 28rem;

    margin-top: 12rem;
    
  }

`;

export const HeaderForm = styled.header`
  width: 100%;
  height: 52px;

  background-color: ${props => props.theme["secondary"]};

  display: flex;
  justify-content: center;
  align-items: center;

  border-top-left-radius: 4px;
  border-top-right-radius: 4px;

`;

export const Content = styled.div`
  background-color: ${props => props.theme["black"]};

  height: fit-content;
  width: 100%;

  padding: 1rem 2rem;

  border-bottom-left-radius: 6px;
  border-bottom-right-radius: 6px;

`

export const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;

  position: relative;

  gap: 6px;

  margin: 1rem auto;

  input {
    width: 100%;
    height: 32px;

    padding: 1.5rem 1rem;

    border: 1px solid transparent;
    border-radius: 4px;

  }

  label {
    font-weight: 400;
    font-size: .875rem;

  }

  svg {
    position: absolute;

    top: 50%;
    right: 15px;

    cursor: pointer;

  }

`;

export const BaseButton = styled.button`
  width: 100%;
  height: 44px;

  color: ${props => props.theme["white"]};

  font-size: 1rem;
  font-family: "Roboto", sans-serif, monospace;
  font-weight: 400;

  letter-spacing: 1px;

  border: 0;
  border-radius: 6px;

  cursor: pointer;

`;

export const LoginButton = styled(BaseButton)`
  background-color: ${props => props.theme["primary"]};
  margin: 1rem auto 1rem auto;

  &:hover {
    opacity: .9;

  }

`;
