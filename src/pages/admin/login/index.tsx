import { Container, Content, HeaderForm, InputContainer, LoginButton } from "../../../components/Login/styles/styles";
import { useForm } from "react-hook-form";
import { EnvelopeSimple, Eye, EyeClosed } from "phosphor-react";

import { useState } from "react";

export default function Login () {
  const [showPass, setShowPass] = useState<boolean>(false);

  const { handleSubmit, register } = useForm();

  const handleSignIn = (data: any) => {

  }

  return (
    <Container>
      <form onSubmit={handleSubmit(handleSignIn)}>
        <HeaderForm>
          App Admin
        </HeaderForm>
        <Content>
          <InputContainer>
            <label htmlFor="email">E-mail</label>
            <input name="email" id="email" type={"email"} autoComplete="off" required />
            <EnvelopeSimple color="#D9D9D9" size={20} /> 
          </InputContainer>
          <InputContainer>
            <label htmlFor="password">Password</label>
            <input name="password" id="password" type={showPass ? "text" : "password"} required />
            {
              showPass ? <Eye onClick={() => setShowPass(state => !state)} size={20} color="#D9D9D9" /> : 
              <EyeClosed onClick={() => setShowPass(state => !state)} size={20} color="#D9D9D9" />
            }
          </InputContainer>
          <LoginButton type="submit">
            Entrar
          </LoginButton>
        </Content>
      </form>
    </Container>
  )

}