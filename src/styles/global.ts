import { createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    color: ${props => props.theme["body-fg"]};
  
    box-sizing: border-box;
  
    font-size: 1rem;

    font-family: "Roboto", sans-serif;

  }

  html, body, input, textarea {
    border: 0;

    background-color: ${props => props.theme["darkened-bg"]};

  }

  :focus {
    outline: none;

    border-color: ${props => props.theme["secondary"]} !important;
 
  }

`;
